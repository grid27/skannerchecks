package com.samberi.mobile.skanner_check;

import android.content.Context;

import org.json.*;
import com.loopj.android.http.*;
import java.util.HashMap;
import cz.msebera.android.httpclient.Header;


public class RestAsyncHttpClient {

    private static String BASE_URL;
    private static AsyncHttpClient client;
    private Context context;
    private static RestAsyncHttpClient _instance;

    public static RestAsyncHttpClient getInstance(Context context) {
        if (_instance == null) {
            _instance = new RestAsyncHttpClient(context);
        }
        return _instance;
    }

    public RestAsyncHttpClient(Context context) {
        BASE_URL = context.getResources().getString(R.string.base_url);
        client = new AsyncHttpClient(true, 80, 443);
        this.context = context;
    }

    public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public void getCheckInfo(String paramsStr) {
        HashMap data = new HashMap();
        String[] _arr = paramsStr.split("&");
        for (int i = 0; i < _arr.length; i++) {
            String[] kv = _arr[i].split("=");
            switch (kv[0]) {
                case "fn":
                    data.put("fiscal_drive", kv[1]);
                    break;
                case "fp":
                    data.put("fiscal_sign", kv[1]);
                    break;
                case "i":
                    data.put("fiscal_document", kv[1]);
                    break;
            }
        }

        RequestParams params = new RequestParams(data);

        get(context.getResources().getString(
                R.string.check_info), params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                ((MainActivity)context).saveCheck(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                ((MainActivity)context).showToast(R.string.get_check_info_err);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                ((MainActivity)context).showToast(R.string.get_check_info_err);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  String responseString, Throwable throwable) {
                ((MainActivity)context).showToast(R.string.get_check_info_err);
            }
        });
    }
}




