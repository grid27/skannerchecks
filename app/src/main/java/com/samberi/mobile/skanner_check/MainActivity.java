package com.samberi.mobile.skanner_check;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import com.samberi.mobile.skanner_check.sql.Check;
import com.samberi.mobile.skanner_check.sql.Ware;

import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private Button scanBtn;
    private ListView lvHistory;
    public List<Check> history;
    private String lastScanData;
    private Context activity;
    private DatabaseManager dbCacheManager;
    private RestAsyncHttpClient rest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = this;
        this.dbCacheManager = DatabaseManager.getInstance(this);
        this.rest = RestAsyncHttpClient.getInstance(this);

        dbCacheManager.getHistory();
        setContentView(R.layout.activity_main);
        createScanBtn();
    }

    protected void createListHistory(List<Check> checks) {
        lvHistory = (ListView) findViewById(R.id.lvHistory);
        history = checks;
        lvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Check check = (Check)adapterView.getItemAtPosition(i);
                showToast("Click! " + check.operator);
                Intent intent = new Intent(activity, CheckInfoActivity.class);
                intent.putExtra("check_barcode", check.barcode);
                startActivity(intent);
            }
        });
        lvHistory.setAdapter(new HistoryListAdapter(this, history));
    }

    protected void createScanBtn() {
        scanBtn = (Button) findViewById(R.id.scan_btn);
        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanNow(view);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                showToast(R.string.cancel_scan);
            }
            else {
                lastScanData = result.getContents();
                dbCacheManager.getCheck(lastScanData);
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void getCheckInfo() {
        rest.getCheckInfo(lastScanData);
    }

    public void saveCheck(JSONObject response) {
//        history.add(check.rival + " " + check.dateTime + " " + check.totalSum.toString())
//        listHistoryAdapter.notifyDataSetChanged();
        dbCacheManager.addCheck(lastScanData, response);
    }

    protected void onCheckAdded() {
    }

    protected void onDataNotAvailable(Throwable e) {
        showToast(e.getMessage());
    }

    public void scanNow(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt(getMainActivityResourceString(R.string.scan_prompt));
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }

    public void showToast(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int code) {
        Toast.makeText(activity, getMainActivityResourceString(code), Toast.LENGTH_SHORT).show();
    }

    public String getMainActivityResourceString(int code) {
        return activity.getResources().getString(code);
    }
}
