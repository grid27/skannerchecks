package com.samberi.mobile.skanner_check.sql;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;


@Entity(tableName = "check")
public class Check {

    public Check(
            String barcode,
            Float totalSum,
            String dateTime,
            String rival,
            String operator,
            String fiscalDocument,
            String fiscalDrive,
            String fiscalSign
    ) {
        this.barcode = barcode;
        this.totalSum = totalSum;
        this.dateTime = dateTime;
        this.rival = rival;
        this.operator = operator;
        this.fiscalDocument = fiscalDocument;
        this.fiscalDrive = fiscalDrive;
        this.fiscalSign = fiscalSign;
    }

    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "barcode")
    public String barcode;

    @ColumnInfo(name = "totalSum")
    public Float totalSum;

    @ColumnInfo(name = "dateTime")
    public String dateTime;

    @ColumnInfo(name = "rival")
    public String rival;

    @ColumnInfo(name = "operator")
    public String operator;

    @ColumnInfo(name = "fiscalDocument")
    public String fiscalDocument;

    @ColumnInfo(name = "fiscalDrive")
    public String fiscalDrive;

    @ColumnInfo(name = "fiscalSign")
    public String fiscalSign;

    @Ignore
    public List<Ware> wares;
}

