package com.samberi.mobile.skanner_check;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.samberi.mobile.skanner_check.sql.Check;

import java.util.List;


public class HistoryListAdapter extends BaseAdapter {
    Context context;
    List<Check> data;
    private static LayoutInflater inflater = null;

    public HistoryListAdapter(Context context, List<Check> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (vi == null)
            vi = inflater.inflate(R.layout.check_row, null);

        TextView text = (TextView) vi.findViewById(R.id.text);
        TextView header = (TextView) vi.findViewById(R.id.header);
        Check item = data.get(position);
        String headerText = (item.rival.length() > 0 ? item.rival: "No name");
        headerText = headerText + " | " + item.totalSum.toString() + " р.";
        header.setText(headerText);

        text.setText(data.get(position).dateTime);
        return vi;
    }
}
