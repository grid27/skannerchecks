package com.samberi.mobile.skanner_check.sql;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.ForeignKey;
import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(
        tableName = "ware",
        foreignKeys = @ForeignKey(
            entity = Check.class,
            parentColumns = "id",
            childColumns = "parentId",
            onDelete=CASCADE
        ),
        indices=@Index(value="parentId")
)
public class Ware {

    public Ware(
            String name,
            Float price,
            Float quantity,
            long parentId
    ) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.parentId = parentId;
    }

    @PrimaryKey( autoGenerate = true )
    public long id;
    public long parentId;

    @ColumnInfo( name = "name")
    public String name;

    @ColumnInfo( name = "price")
    public Float price;

    @ColumnInfo( name = "quantity")
    public Float quantity;

}
