package com.samberi.mobile.skanner_check;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.samberi.mobile.skanner_check.sql.Check;
import com.samberi.mobile.skanner_check.sql.CheckDao;
import com.samberi.mobile.skanner_check.sql.Ware;
import com.samberi.mobile.skanner_check.sql.WareDao;


@Database(entities = {Check.class, Ware.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CheckDao checkDao();
    public abstract WareDao wareDao();
}