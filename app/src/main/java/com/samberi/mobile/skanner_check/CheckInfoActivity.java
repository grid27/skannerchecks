package com.samberi.mobile.skanner_check;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.samberi.mobile.skanner_check.sql.Check;
import com.samberi.mobile.skanner_check.sql.Ware;

import java.util.List;

public class CheckInfoActivity extends AppCompatActivity {

    private Check check;
    private DatabaseManager dbCacheManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_info);
        Intent intent = this.getIntent();
        String checkBarcode = intent.getStringExtra("check_barcode");
        dbCacheManager = DatabaseManager.getInstance(this);
        dbCacheManager.getCheck(checkBarcode);
    }

    protected void appendWares(List<Ware> wares) {
        check.wares = wares;
    }

    protected void getCheck(Check check) {
        this.check = check;
        TextView title = (TextView) findViewById(R.id.title);
        title.setText(check.operator);
        dbCacheManager.getWares(check.id);

    }

}
