package com.samberi.mobile.skanner_check;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.samberi.mobile.skanner_check.sql.Check;
import com.samberi.mobile.skanner_check.sql.Ware;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.MaybeObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class DatabaseManager {

    private static final String DB_NAME = "Data.db";
    private Context context;
    private static DatabaseManager _instance;
    private AppDatabase db;

    public static DatabaseManager getInstance(Context context) {
        if (_instance == null) {
            _instance = new DatabaseManager(context);
        }
        _instance.context = context;
        return _instance;
    }

    public DatabaseManager(Context context) {
        db = Room.databaseBuilder(context, AppDatabase.class, DB_NAME).build();
    }

    public void getWares(long checkId) {
        db.wareDao()
          .getWares(checkId)
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Consumer<List<Ware>>() {
              @Override
              public void accept(@NonNull List<Ware> wares) throws Exception {
                  if (context instanceof CheckInfoActivity) {
                      ((CheckInfoActivity) context).appendWares(wares);
                  }
              }
          });
    }

    public void getCheck(String barcode) {
        db.checkDao()
                .getCheck(barcode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new MaybeObserver<Check>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onSuccess(Check check) {
                        // If has check in history
                        if (context instanceof MainActivity){
                            ((MainActivity)context).showToast(R.string.exist_check);
                        }
                        else if (context instanceof CheckInfoActivity) {
                            ((CheckInfoActivity)context).getCheck(check);
                        }
                    }
                    @Override
                    public void onComplete() {
                        // If check not found in history
                        if (context instanceof MainActivity) {
                            ((MainActivity)context).getCheckInfo();
                        }
                    }
                });
    }

    public void getHistory() {
        db.checkDao()
              .getAll()
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new Consumer<List<Check>>() {
                    @Override
                    public void accept(List<Check> checks) throws Exception {
                        ((MainActivity)context).createListHistory(checks);
                    }
                });
    }

    public void addCheck(
            final String barcode,
            final JSONObject data
    ) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                Check check = new Check(
                        barcode,
                        Float.valueOf(data.getString("total_sum")),
                        data.getString("date_time"),
                        data.getString("rival"),
                        data.getString("operator"),
                        data.getString("fiscal_document"),
                        data.getString("fiscal_drive"),
                        data.getString("fiscal_sign")
                );
                long checkId = db.checkDao().insert(check);
                JSONArray wares = data.getJSONArray("wares");

                for (int i = 0; i < wares.length(); i++) {
                    JSONObject wareData = wares.getJSONObject(i);
                    Ware ware = new Ware(
                            wareData.getString("name"),
                            Float.valueOf(wareData.getString("price")),
                            Float.valueOf(wareData.getString("quantity")),
                            checkId
                    );
                    db.wareDao().insert(ware);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onComplete() {
                ((MainActivity)context).onCheckAdded();
            }

            @Override
            public void onError(Throwable e) {
                ((MainActivity)context).onDataNotAvailable(e);
            }
        });
    }

}