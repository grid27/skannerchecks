package com.samberi.mobile.skanner_check.sql;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
import io.reactivex.Flowable;


@Dao
public interface WareDao {

    @Query("SELECT * FROM `ware` WHERE parentId = :checkId")
    Flowable<List<Ware>> getWares(long checkId);

    @Insert
    long insert(Ware ware);

    @Delete
    void delete(Ware ware);
}
