package com.samberi.mobile.skanner_check.sql;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
import io.reactivex.Flowable;
import io.reactivex.Maybe;


@Dao
public interface CheckDao {

    @Query("SELECT * FROM `check`")
    Flowable<List<Check>> getAll();

    @Query("SELECT * FROM `check` WHERE barcode = :barcode LIMIT 1")
    Maybe<Check> getCheck(String barcode);

    @Insert
    long insert(Check check);

    @Delete
    void delete(Check check);
}
